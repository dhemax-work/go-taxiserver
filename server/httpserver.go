package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/rs/cors"
	"io/ioutil"
	"log"
	"net/http"
	"time"
)

var isWebServerOnline = false

type TestRequest struct {
	Speed int `json:"speed"`
}

type Response struct {
	Code         int         `json:"code"`
	Message      string      `json:"message"`
	JSONResponse interface{} `json:"response"`
}

var LastSpeed int
var response Response

func setupResponse(w *http.ResponseWriter) {
	(*w).Header().Set("Access-Control-Allow-Origin", "*")
	(*w).Header().Set("Access-Control-Allow-Methods", "POST, GET")
	(*w).Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
	(*w).Header().Set("Content-Type", "application/json")
}

//BodyToJson ...
func BodyToJson(r *http.Request) []byte {
	bodyB, _ := ioutil.ReadAll(r.Body)
	bodyStr := string(bytes.Replace(bodyB, []byte("\r"), []byte("\r\n"), -1))
	bytesreturned := []byte(bodyStr)

	return bytesreturned
}

func EndPointAPI(port int){
	realPort := fmt.Sprintf(":%v", port)

	if !isWebServerOnline {
		fmt.Printf("\n== [Starting Web Server (WS)] ==")
		time.Sleep(time.Second * 1)
		mux := http.NewServeMux()
		fmt.Printf("\n== [WS: [38%%] Load Mux...] ==")

		mux.HandleFunc("/", TestData)             //GET [CHECKED]
		mux.HandleFunc("/speed", PostTest)

		fmt.Printf("\n== [WS: [64%%] Loaded Methods ...] ==")
		time.Sleep(time.Second * 2)
		handler := cors.AllowAll().Handler(mux)
		fmt.Printf("\n== [WS: [73%%] CORS Loaded...] ==")

		time.Sleep(time.Second * 1)

		go func() {
			log.Fatal(http.ListenAndServe(realPort, handler))
		}()

		fmt.Printf("\n== [WS: [100%%] Server Started (port %s)...] ==\n", realPort)
		isWebServerOnline = true
	}
}

func PostTest(writer http.ResponseWriter, request *http.Request) {
	setupResponse(&writer)
	bytes := BodyToJson(request)
	if request.Method == "POST" {
		var result TestRequest
		json.Unmarshal(bytes, &result)
		LastSpeed = result.Speed
		response = Response{
			Code:         200,
			Message:      fmt.Sprintf("Speed received: %v", result.Speed),
			JSONResponse: result,
		}
		//bytes := BodyToJson(request)
	}else{
		response = Response{
			Code:         300,
			Message:      "Invalid Method",
			JSONResponse: nil,
		}
	}

	json.NewEncoder(writer).Encode(response)
}

func TestData(writer http.ResponseWriter, request *http.Request) {
	setupResponse(&writer)
	if request.Method == "GET" {
		response = Response{
			Code:         200,
			Message:      "LastSpeed",
			JSONResponse: LastSpeed,
		}
		//bytes := BodyToJson(request)
	}else{
		response = Response{
			Code:         300,
			Message:      "Invalid Method",
			JSONResponse: nil,
		}
	}

	json.NewEncoder(writer).Encode(response)
}