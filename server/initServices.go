package main

import (
	"log"
	"os"
	"os/exec"
	"runtime"
	"time"
)

var EndPointPort = 8100
var ServerEventPort = 8008

func main() {

	ClearConsole()
	//SocketPort := 3333
	Log("Starting Services....")
	time.Sleep(time.Second * 2)

	EndPointAPI(EndPointPort)
	time.Sleep(time.Second * 2)
	matchmaking()
	time.Sleep(time.Second * 1)
	StartEventServer()

}

func Log( msg string) {
	l := log.New(os.Stdout, "", 0)
	l.SetPrefix(time.Now().Format("[2006-01-02 - 15:04:05]" + " "))
	l.Print(msg+"\n")
}

var clear map[string]func() //create a map for storing clear funcs

func init() {
	clear = make(map[string]func()) //Initialize it
	clear["linux"] = func() {
		cmd := exec.Command("clear") //Linux example, its tested
		cmd.Stdout = os.Stdout
		cmd.Run()
	}
	clear["windows"] = func() {
		cmd := exec.Command("cmd", "/c", "cls") //Windows example, its tested
		cmd.Stdout = os.Stdout
		cmd.Run()
	}
}

func ClearConsole() {
	value, ok := clear[runtime.GOOS] //runtime.GOOS -> linux, windows, darwin etc.
	if ok {                          //if we defined a clear func for that platform:
		value() //we execute it
	} else { //unsupported platform
		panic("Your platform is unsupported! I can't clear terminal screen :(")
	}
}
