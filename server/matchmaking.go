package main

import (
	"fmt"
	"time"
)

var WhitelistClient []Client

func CreateTravel(client Client, destination GPSLocation) MatchClientVehicle{
	newMatch := MatchClientVehicle{
		TravelID:		fmt.Sprintf("abc-%v",client.ClientID),
		Client:        client,
		Vehicle:       Vehicle{},
		StartLocation: client.ClientPosition,
		EndLocation:   client.ClientDestination,
		Markers:       nil,
		Status:        0,
	}

	InRouteList = append(InRouteList, newMatch)
	Log(fmt.Sprintf("New travel create [%v] by %v", newMatch.TravelID, client.ClientID))
	return newMatch
}

func matchmaking(){
	go func(){
		time.Sleep(time.Second * 6)
		Log("Starting ClientVehicle Match Making")
		for {
			time.Sleep(time.Millisecond * 400)
			if len(VehicleList) < 1 {
				Log("No available vehicles for depart")
			}else{
				if len(WhitelistClient) > 0{
					for _, n := range WhitelistClient {
						for i, v := range VehicleList {
							if v.Status == 0 {
								newMatch := SearchTravel(n)
								fmt.Printf("\n MATCH %v \n", newMatch)
								//fmt.Printf("\n CLIENT %v \n", n)
								if newMatch.Client.ClientID != "" {
									VehicleList[i].Status = 1
									newMatch.Vehicle = v
									newMatch.Status = 1

									time.Sleep(time.Millisecond * 100)
									RemoveClientFromWhiteList(i)
									Log(fmt.Sprintf("\nMATCH: Vehicle %v with client %v", v.VehicleID, n.ClientID))
									time.Sleep(time.Millisecond * 100)
									matchmaking()
									break
								}
								continue
							}else{
								continue
							}
						}
					}
				}else{
					Log("No clients waiting...")
					time.Sleep(time.Second * 1)
				}
			}
		}
	}()
}
