package main

import "fmt"

func AddClientToWaitList(client string, dest GPSLocation){
	var Client Client
	Client = SearchClient(client)
	if Client.ClientID != "" {
		Client.ClientDestination = dest
		WhitelistClient = append(WhitelistClient, Client)
		fmt.Printf("\nClient %v waiting for a car", Client.ClientID)
	}
}

func RemoveClientFromWhiteList(index int){
	for i, _ := range WhitelistClient{
		if i == index {
			WhitelistClient = append(WhitelistClient[:index], WhitelistClient[index+1:]...)
			if index > 0 {
				index = index -1
			}
			continue
		}
		index++
	}
}

func SearchTravel(client Client) MatchClientVehicle {
	if HaveTravel(client){
		for i, _ := range InRouteList{
			if InRouteList[i].Client.ClientID == client.ClientID{
				return InRouteList[i]
			}
		}
	}
	return MatchClientVehicle{}
}

func HaveTravel(client Client) bool {
	for i, _ := range InRouteList{
		if InRouteList[i].Client.ClientID == client.ClientID {
			return true
		}
	}
	return false
}

func SearchClient(clientID string) Client {
	if HaveClient(clientID){
		for _, n := range ClientList{
			if clientID == n.ClientID{
				return n
			}
		}
	}
	return Client{}
}

func HaveClient(ClientID string) bool {
	for i, _ := range ClientList {
		//Log(fmt.Sprintf("Clientes: %v", ClientList))
		//Log(fmt.Sprintf("ClientID [%v] === ListID [%v]", ClientID, ClientList[i].ClientID))
		if ClientID == ClientList[i].ClientID {
			return true
		}
	}
	return false
}

func IsClientInRoute(ClientID string) bool{
	for i, _ := range UserInRoute {
		if ClientID == UserInRoute[i].ClientID{
			return true
		}
	}
	return false
}
